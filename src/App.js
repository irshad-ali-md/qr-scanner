// import React, { useState } from "react";
// import { QrReader } from "react-qr-reader";

// const App = () => {
//   const [data, setData] = useState("No result");

//   return (
//     <>
//       <h1>QR Scanner</h1>
//       <QrReader
//         onResult={(result, error) => {
//           console.log("result", result);
//           console.log("error", error);

//           if (!!result) {
//             setData(result?.text);
//           }

//           if (!!error) {
//             console.info(error);
//           }
//         }}
//         constraints={{
//           facingMode: "user",
//         }}
//         style={{ width: "100%" }}
//       />
//       <p>{data}</p>
//     </>
//   );
// };

import { useState } from "react";
import Html5QrcodePlugin from "./Html5QrcodePlugin";

const App = () => {
  const [data, setData] = useState("");

  const onNewScanResult = (decodedText, decodedResult) => {
    setData(decodedText);
  };

  return (
    <div className="App">
      <Html5QrcodePlugin
        fps={10}
        qrbox={250}
        disableFlip={false}
        qrCodeSuccessCallback={onNewScanResult}
      />
      {data ? (
        <div style={{ textAlign: "center", marginTop: 15 }}>{data}</div>
      ) : null}
    </div>
  );
};

export default App;

// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
